# What is this?
KYC is terrible for privacy. So how do you buy or sell Bitcoin? Solution is simpler than it looks: just create a new group with your friends using one of the e2e encrypted messengers and exchange peer-to-peer without KYC.

This isn't software. It's just a protocol for people to follow when starting their own exchange groups.


## The protocol

### Starting a exchange group:
- Create a group in e2e encrypted messaging app (ie. Signal, Threema).
- Set messages to auto-disappear in 1 week or less.
- Each new user needs to be approved by the admin.
- Link this protocol in the group description. If you want to make changes, fork it.
- Don't create [Web of Trust](https://en.wikipedia.org/wiki/Web_of_trust) deeper than 2.


### Basic rules of the exchange group:
- The first rule of the group is you don't talk about the group.
- Maker posts offer as a message in the group. Include what do you want, how much and what method, e.g. "I'll buy BTC (LN/onchain) for $250; market price; over Venmo or in-person cash in San Francisco".
- Person who wants to take the offer contacts the maker *directly* over private message where they figure out details (e.g. share LN invoice...).
- Fiat pays first.
- Maker deletes the message after the offer has been filled (or marks it with smiley...)
- No non-offer messages in the main exchange group


### Adding new members to the exchange group:
- Any existing member can suggest adding new people by texting the admin along with justification of trust.
- Only add people that you know and trust


### Create a support/general group:
- Create another parallel group where people can just chat about whatever and potentially ask about the exchange group
- The support general group can be more open 


### Resolving disputes:
- TODO


### Adding or changing admins:
- TODO


## FAQ

- Why not just Coinbase?
    - Because it sucks, just like every other KYC exchange. They get hacked all the time and then everyone knows where you live and how much bitcoin you own.

- What the heck is KYC/AML?
    - *Know Your Customer* and *Anti-Money Laundering*. An evil set of user exclusion policies usually enforced by governments. In practice the policies are used to exclude specific users (e.g. people without ID, people struggling economically, people from unfavored countries) from being able to participate in the economy. The policies (intentionally?) don't reduce financial crime, even though that's what the marketing may tell you. The application of KYC/AML policies further results in massive breaches of user privacy and leaks of personal information. 

- What's the difference between this and Bisq?
    - [Bisq](https://bisq.network/) is awesome. It's a software, also enabling peer-to-peer exchange. It has built-in support for escrow and dispute resolution through a third person. It works well, but it has fees. Also, since you need to use some fiat-payment such as Zelle or Venmo, the relevant bank has that information.

- Where to find people?
    - Local Bitcoin meetups
    - Talk to your friends


## Contributing

You've been running your own group and has some ideas for improvements that other people could learn from? Fantastic! Send a [PR](https://gitlab.com/sovereign-individuals/p2p-exchange-protocol/-/merge_requests/new) with your suggestion.


## Other resources
- https://github.com/taxmeifyoucan/p2p-trading/ - practical learnings from a group following similar protocol. 


## Credit

Copied from Stacker.news [post](https://stacker.news/items/7424) by [@nout](https://stacker.news/nout), so that we can continue refining it.
